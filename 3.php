<?php
//programa que va restando a un numero de dos en dos mientras sea mayor de 10
$numero = 45;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>

<body>
    <?php
    //procesamiento
    while ($numero > 10) :
        $numero = $numero - 2;
        echo "<br>$numero";
    endwhile;
    ?>
</body>

</html>