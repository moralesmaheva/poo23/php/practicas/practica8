<?php
//ciclo for que muestre en pantalla los numeros del 1 al 150 cada uno en un div
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1</title>
</head>

<body>
    <?php
    //procesamiento e impresion
    for ($c = 1; $c <= 150; $c++) {
        echo "<div>{$c}</div>";
    }
    ?>
</body>

</html>