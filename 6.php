<?php
//crar array con 4 notas y calcular la nota media
$notas = [
    6, 3, 6.5, 7,
];
$suma = array_sum($notas);
$numNotas = count($notas);

//procesamiento
$media = $suma / $numNotas;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 6</title>
</head>

<body>
    <!-- impresion -->
    <?php
    //para imprimir las notas por pantalla utilizamos un for
    for ($c = 0; $c < $numNotas; $c++) {
        echo "<br>{$notas[$c]}";
    }
    ?>
    <div>La nota media es <?= $media ?></div>
</body>

</html>