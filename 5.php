<?php
//crear un array con 4 notas y mostrarlas
$notas = [
    5, 8, 4.3, 9.5,
];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5</title>
</head>

<body>
    <?php
    //procesamiento con for
    for ($c = 0; $c < count($notas); $c++) {
        echo "$notas[$c] <br>";
    }
    //procesamiento con un foreach
    foreach ($notas as $valor) {
        echo "<br> $valor";
    }

    ?>
</body>

</html>