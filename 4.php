<?php
//programa que devuelve la suma de los numeros impares hasta un numero dado
$numero = 40;
$suma = 0;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>

<body>
    <?php
    //procesamiento
    for ($c = 0; $c < $numero; $c++) {
        if ($c % 2 == 1) {
            $suma = $suma + $c;
            echo "<br>$c";
        }
    }
    ?>

    <div>La suma de los numeros impares es <?= $suma ?></div>
</body>

</html>