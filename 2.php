<?php
//crear bucle while para mostrar los numeros del 1 al 150
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>

<body>
    <?php
    //procesamiento
    $c = 1;
    while ($c <= 150) :
        echo " $c <br>";
        $c++;
    endwhile;
    ?>
</body>

</html>